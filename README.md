RGB-to-Component Converter
==========================

This board converts RGB video from an Apple IIGS to component video that can
drive many (though not all) TVs with component input.  The limiting factor
for compatibility appears to be that the IIGS doesn't produce 480i video,
but something often labeled "240p": 60000/1001 fps progressive-scan video
instead of 30000/1001 fps interlaced.

I originally created this board with the Upverter online ECAD system back
around 2013-2014.  Upverter is still around today and my files are still
accessible, but it'd be nice to have the project in a form that can be
manipulated with offline, open-source tools.  This project will replicate
the original, this time in KiCad.

